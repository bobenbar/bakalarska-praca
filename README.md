# Bachelor thesis

In Jupyter notebooks directory are uploaded source code of notebooks used in bachelor thesis. 

Notebooks are divided by architectures type, library and dataset. 

Notebooks are based on examples from documentations of libraries ([link](https://pytorch-geometric.readthedocs.io/en/latest/) ).


Graphs of architectures

![Graphs of architectures!](/Images/arch1-5.png "Graphs of architectures")
